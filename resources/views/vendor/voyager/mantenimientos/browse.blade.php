@extends('voyager::master')
@section('content')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

  <style>
  /* Makes images fully responsive */

  .img-responsive,
  .thumbnail > img,
  .thumbnail a > img,
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    display: block;
    width: 100%;
    height: auto;
  }

  /* ------------------- Carousel Styling ------------------- */

  .carousel-inner {
    border-radius: 5px;
    height: 250px;
  overflow: hidden;
  }

  .carousel-caption {
    background-color: rgba(0,0,0,.5);
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 10;
    padding: 0 0 10px 25px;
    color: #fff;
    text-align: left;
  }

  .carousel-indicators {
    position: absolute;
    bottom: 0;
    right: 0;
    left: 0;
    width: 100%;
    z-index: 15;
    margin: 0;
    padding: 0 25px 25px 0;
    text-align: right;
  }

  .carousel-control.left,
  .carousel-control.right {
    background-image: none;
  }


  /* ------------------- Section Styling - Not needed for carousel styling ------------------- */

  .section-white {
     padding: 10px 0;
  }

  .section-white {
    background-color: #fff;
    color: #555;
  }

  @media screen and (min-width: 387px) {

    .section-white {
       padding: 1.5em 0;
    }

  }

  @media screen and (min-width: 992px) {

    .container {
      max-width: 930px;
    }

  }

  </style>

<section class="section-white">
  <div class="container">
      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    <div class="row">
  		<div class="col-md-4">
  			<div class="thumbnail">
  					<a href="{{ route('listaProcesos') }}">
  					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="Lights" style="width:30%">
  					<div class="caption">
  						<p>Mantenimiento de catalogos procesos.</p>
  					</div>
  				</a>
  			</div>
  		</div>
  		<div class="col-md-4">
  			<div class="thumbnail">
  					<a href="{{ route('listaParametros') }}">
  					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="Lights" style="width:30%">
  					<div class="caption">
  						<p>Mantenimiento de catalogos parametros.</p>
  					</div>
  				</a>
  			</div>
  		</div>
  		<div class="col-md-4">
  			<div class="thumbnail">
  					<a href="{{ route('listaProcUsuarios') }}">
  					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="Lights" style="width:30%">
  					<div class="caption">
  						<p>Mantenimiento de catalogos Procesos por Usuario.</p>
  					</div>
  				</a>
  			</div>
  		</div>
  		<div class="col-md-4">
  			<div class="thumbnail">
  					<a href="{{ route('listaProcConfig') }}">
  					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\mejorarprocesos.jpg" alt="Lights" style="width:30%">
  					<div class="caption">
  						<p>Mantenimiento de catalogos Procesos Configuraciones.</p>
  					</div>
  				</a>
  			</div>
  		</div>
			<div class="col-md-4">
				<div>
				<input type="checkbox" checked="true" class="probeProbe" />
				</div>
				<div class="caption">
					<p>Jiterrbit on/off</p>
				</div>

			</div>

			<!-- Material checked -->

	 </div>
</div>
</section>

<script>
  $(function() {

	        $(document).ready(function() {

							$('.probeProbe').bootstrapSwitch('state', true);

					  	$('.probeProbe').on('switchChange.bootstrapSwitch', function (event, state) {

						    //alert(state);
								  $.ajax({
										 url: '/updateJitterBit',
										 method: 'GET',
										 data: {accion: state},
										 success: function(data){
												 alert("Auto Reply has been Turned On");
										 },
										 error: function(){}
								 });
						});
	        });


  })


</script>

@endsection
