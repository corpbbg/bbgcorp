@extends('voyager::master')
@section('content')
		<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">




<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>




  <style>
  /* Makes images fully responsive */

  .img-responsive,
  .thumbnail > img,
  .thumbnail a > img,
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    display: block;
    width: 100%;
    height: auto;
  }

  /* ------------------- Carousel Styling ------------------- */

  .carousel-inner {
    border-radius: 5px;
    height: 250px;
  overflow: hidden;
  }

  .carousel-caption {
    background-color: rgba(0,0,0,.5);
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 10;
    padding: 0 0 10px 25px;
    color: #fff;
    text-align: left;
  }

  .carousel-indicators {
    position: absolute;
    bottom: 0;
    right: 0;
    left: 0;
    width: 100%;
    z-index: 15;
    margin: 0;
    padding: 0 25px 25px 0;
    text-align: right;
  }

  .carousel-control.left,
  .carousel-control.right {
    background-image: none;
  }


  /* ------------------- Section Styling - Not needed for carousel styling ------------------- */

  .section-white {
     padding: 10px 0;
  }

  .section-white {
    background-color: #fff;
    color: #555;
  }

  @media screen and (min-width: 387px) {

    .section-white {
       padding: 1.5em 0;
    }

  }

  @media screen and (min-width: 992px) {

    .container {
      max-width: 930px;
    }

  }

  </style>

	<!-- add calander in this div -->
	<div class="container">
	 <div class="row">
	<div id="calendar"></div>

	</div>
	</div>


	<!-- Modal  to Add Event -->
	<div id="createEventModal" class="modal fade" role="dialog">
	 <div class="modal-dialog">

	 <!-- Modal content-->
	 <div class="modal-content">
	 <div class="modal-header">
	 <button type="button" class="close" data-dismiss="modal">×</button>
	 <h4 class="modal-title">Add Event</h4>
	 </div>
	 <div class="modal-body">
	 <div class="control-group">
	 <label class="control-label" for="inputPatient">Event:</label>
	 <div class="field desc">
	 <input class="form-control" id="title" name="title" placeholder="Event" type="text" value="">
	 </div>
	 </div>

	 <input type="hidden" id="startTime"/>
	 <input type="hidden" id="endTime"/>



	 <div class="control-group">
	 <label class="control-label" for="when">When:</label>
	 <div class="controls controls-row" id="when" style="margin-top:5px;">
	 </div>
	 </div>

	 </div>
	 <div class="modal-footer">
	 <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	 <button type="submit" class="btn btn-primary" id="submitButton">Save</button>
	 </div>
	 </div>

	 </div>
	</div>

	<!-- Modal to Event Details -->
	<div id="calendarModal" class="modal fade">
	<div class="modal-dialog">
	 <div class="modal-content">
	 <div class="modal-header">
	 <button type="button" class="close" data-dismiss="modal">×</button>
	 <h4 class="modal-title">Event Details</h4>
	 </div>
	 <div id="modalBody" class="modal-body">
	 <h4 id="modalTitle" class="modal-title"></h4>
	 <div id="modalWhen" style="margin-top:5px;"></div>
	 </div>
	 <input type="hidden" id="eventID"/>
	 <div class="modal-footer">
	 <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	 <button type="submit" class="btn btn-danger" id="deleteButton">Delete</button>
	 </div>
	 </div>
	</div>
	</div>
	<!--Modal-->


<script>

$(document).ready(function(){
        var calendar = $('#calendar').fullCalendar({  // assign calendar
            header:{
                left: 'prev,next today',
                center: 'title',
                right: 'agendaWeek,agendaDay'
            },
            defaultView: 'agendaWeek',
            editable: true,
            selectable: true,
            allDaySlot: false,

            events: "/crud_calendario?view=1",  // request to load current events


            eventClick:  function(event, jsEvent, view) {  // when some one click on any event
              //  endtime = $.fullCalendar.moment(event.end).format('h:mm');
              //  starttime = $.fullCalendar.moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
                endtime = moment(event.end).format('h:mm');
                starttime = moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
                var mywhen = starttime + ' - ' + endtime;
                $('#modalTitle').html(event.title);
                $('#modalWhen').text(mywhen);
                $('#eventID').val(event.id);
                $('#calendarModal').modal();
            },

            select: function(start, end, jsEvent) {  // click on empty time slot
							alert('seleccionados');
                //endtime = $.fullCalendar.moment(event.end).format('h:mm');
                //starttime = $.fullCalendar.moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
								endtime = moment(event.end).format('h:mm');
                starttime = moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
                var mywhen = starttime + ' - ' + endtime;
                start = moment(start).format();
                end = moment(end).format();
                $('#createEventModal #startTime').val(start);
                $('#createEventModal #endTime').val(end);
                $('#createEventModal #when').text(mywhen);
                $('#createEventModal').modal('toggle');
           },
           eventDrop: function(event, delta){ // event drag and drop
               $.ajax({
                   url: '/crud_calendario',
                   data: 'action=update&title='+event.title+'&start='+moment(event.start).format()+'&end='+moment(event.end).format()+'&id='+event.id ,
                   type: "POST",
                   success: function(json) {
                   //alert(json);
                   }
               });
           },
           eventResize: function(event) {  // resize to increase or decrease time of event
               $.ajax({
                   url: '/crud_calendario',
                   data: 'action=update&title='+event.title+'&start='+moment(event.start).format()+'&end='+moment(event.end).format()+'&id='+event.id,
                   type: "POST",
                   success: function(json) {
                       //alert(json);
                   }
               });
           }
        });

       $('#submitButton').on('click', function(e){ // add event submit
           // We don't want this to act as a link so cancel the link action
           e.preventDefault();
           doSubmit(); // send to form submit function
       });

       $('#deleteButton').on('click', function(e){ // delete event clicked
           // We don't want this to act as a link so cancel the link action
           e.preventDefault();
           doDelete(); //send data to delete function
       });

       function doDelete(){  // delete event
           $("#calendarModal").modal('hide');
           var eventID = $('#eventID').val();
           $.ajax({
               url: '/crud_calendario',
               data: 'action=delete&id='+eventID,
               type: "POST",
               success: function(json) {
                   if(json == 1)
                        $("#calendar").fullCalendar('removeEvents',eventID);
                   else
                        return false;


               }
           });
       }
       function doSubmit(){ // add event
           $("#createEventModal").modal('hide');
           var title = $('#title').val();
           var startTime = $('#startTime').val();
           var endTime = $('#endTime').val();

           $.ajax({
               url: '/crud_calendario',
               data: 'action=add&title='+title+'&start='+startTime+'&end='+endTime,
               type: "POST",
               success: function(json) {
                   $("#calendar").fullCalendar('renderEvent',
                   {
                       id: json.id,
                       title: title,
                       start: startTime,
                       end: endTime,
                   },
                   true);
               }
           });

       }
    });

</script>

@endsection
