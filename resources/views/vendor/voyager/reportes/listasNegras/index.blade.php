@extends('voyager::master')

@section('content')

<style>

	.bg-primary {
			background-color: #19B5FE;
			color : white;
	}

	table.dataTable thead tr {
	  background-color: #82CFFD;
	color : black;
	}

	tfoot tr, thead tr {
		--background: green;
		--color : white;
	}
	tfoot td {
		--font-weight:bold;
	}

	div.container {
	        width: 100%;
	    }

	  #containerGrafica {
	      height: 480px;
	      min-width: 500px;
	      max-width: 800px;
	      margin: 0 auto;
	  }
	  #modalReport {
	      height: 500px;
	      min-width: 310px;
	      max-width: 800px;
	      margin: 0 auto;
	  }

	  @media (min-width: 772px) {
	    .modal-xl {
	      width: 90%;
	     max-width:90%;
	    }
	  }

	#abcd {
	  width:82%;
	  height:100%;
	  --opacity:.95;
	  top:0;
	  padding-top: 5%;
	  display:none;
	  position:fixed;
	  background-color:#ffffff;
	  overflow:auto
	}
	img#close {
	  position:absolute;
	  right:-14px;
	  top:-14px;
	  cursor:pointer
	}

	form {
	  max-width:100%;
	  min-width:100%;

	  font-family:raleway;
	  background-color:#fff
	}
	p {
	  margin-top:0%
	}

	th { font-size: 12px; }
	td { font-size: 11px; }

</style>
	<form action="#" id="form" method="post" name="form">

		<div>
			<button type="button" class="btn btn-danger pull-right" id="menu" name="menu" onclick="retornaMenu();">Regresar Menú</button>
      <div id="containerGrafica"></div>
		</div>
	      <div id="abcd" class="container-fluid">

		        	<h2 class="title" align="center" name="EstadoId" id="EstadoId"></h2>

									<div class="row">

										<div class="modal-header">
					          	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal" id="exit" name="exit" >Regresar Listado Geográfico</button>
					          </div>
										 <div class="col-lg-3">
											 <div class="input-group">
												 	<span class="input-group-addon bg-primary">Estado: </span>
												<div class="input-group">

														<select class="form-control" id="claveSelected">
															<option value="0">Buscar por Estado</option>
															<option value="AGS">Aguascalientes	</option>
															<option value="BC">Baja California	</option>
															<option value="BCS">Baja California Sur	</option>
															<option value="CDM">Ciudad de México	</option>
															<option value="CHI">Chihuahua	</option>
															<option value="CHS">Chiapas	</option>
															<option value="CMP">Campeche	</option>
															<option value="COA">Coahuila	</option>
															<option value="COL">Colima	</option>
															<option value="DF">Distrito Federal	</option>
															<option value="DGO">Durango	</option>
															<option value="GRO">Guerrero	</option>
															<option value="GTO">Guanajuato	</option>
															<option value="HGO">Hidalgo	</option>
															<option value="JAL">Jalisco	</option>
															<option value="MCH">Michoacán	</option>
															<option value="MEX">Estado de México	</option>
															<option value="MOR">Morelos	</option>
															<option value="NAY">Nayarit	</option>
															<option value="NL">Nuevo León	</option>
															<option value="OAX">Oaxaca	</option>
															<option value="PUE">Puebla	</option>
															<option value="QR">Quintana Roo	</option>
															<option value="QRO">Querétaro	</option>
															<option value="SIN">Sinaloa	</option>
															<option value="SLP">San Luis Potosí	</option>
															<option value="SON">Sonora	</option>
															<option value="TAB">Tabasco	</option>
															<option value="TLX">Tlaxcala	</option>
															<option value="TMS">Tamaulipas	</option>
															<option value="VER">Veracruz	</option>
															<option value="YUC">Yucatán	</option>
															<option value="ZAC">Zacatecas	</option>
															<option value="todos">Todos</option>
													  </select>
														<span class="input-group-addon add-on bg-primary">
															<i class="glyphicon glyphicon-search"></i>
														</span>
														<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
														<div class="help-block with-errors"></div>
												</div>

											</div>

										</div>

										 <div class="col-lg-9 ">
		 									<div class="btn-group pull-right">
		 										<div id="buttonsTable1"></div>
		 									</div>
		 							 	</div>
									</div>


	                <table id='tabla_listaNegra' class='display table-striped table-bordered table-hover table-condensed ' style="width:100%">

	                  <thead>
	                    <tr>
	                      <th>Clave SAP</th><th>RFC</th><th>Razón Social</th><th>Sociedad</th><th>Nombre Sociedad</th>
	                      <th>Estado Lista Negra SAP</th><th>Estado Lista Negra BBG</th><th>Fecha Último Movimiento SAP</th>

												<th>Clave Estado</th><th>Municipio</th><th>Tipo Relación</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  </tbody>
	                </table>


					</div>

		</form>


		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script>

 function retornaMenu(){

	 window.location.href = "{{ route('menuFiscal') }}";
 }

var clave_estado=null;
    var arregloserie = new Array();
    var dataJson = null;

		var arregloEdo = {"Aguascalientes": "AGS",
			"Baja California": "BC",
			"Baja California Sur": "BCS",
			"Distrito Federal": "CDM",
			"Chihuahua": "CHI",
			"Campeche": "CMP",
			"Coahuila": "COA",
			"Colima": "COL",
			"Durango": "DGO",
			"Guerrero": "GRO",
			"Guanajuato": "GTO",
			"Hidalgo": "HGO",
			"Jalisco": "JAL",
			"Michoacán": "MCH",
			"México": "MEX",
		  "Chiapas": "CHS",
			"Morelos": "MOR",
			"Nayarit": "NAY",
			"Nuevo León": "NL",
			"Oaxaca": "OAX",
			"Puebla": "PUE",
			"Quintana Roo": "QR",
			"Querétaro": "QRO",
			"Sinaloa": "SIN",
			"San Luis Potosí": "SLP",
			"Sonora": "SON",
			"Tabasco": "TAB",
			"Tlaxcala": "TLX",
			"Tamaulipas": "TMS",
			"Veracruz": "VER",
			"Yucatán": "YUC",
			"Zacatecas": "ZAC"};

      $.ajax({
        type: "GET",
        url: '/listasJson',
        dataType: 'application/json',

        complete: function(data){

	        dataJson = JSON.parse(data.responseText);

					var bandera = true;
					var cantidad = 0;
	        $.each(dataJson,function(key,idCandidato){

	          var processed_json = new Array();

	  				var key = Object.keys(this)[0];
	          var Resultadocandidato= this[key]*1;
	          var key2 = Object.keys(this)[1];
	          var Resultadocandidato2= this[key2];

						if('mx-df' == Resultadocandidato2){
						if(bandera){
							 bandera = false;
							 cantidad = Resultadocandidato;
						}else{
								processed_json.push(Resultadocandidato2, Resultadocandidato+cantidad);
						}

					}else{
						processed_json.push(Resultadocandidato2, Resultadocandidato);
					}

	          arregloserie.push(processed_json);


	        });

	        graficaListas(arregloserie);

        },
        success: function(data){
            alert('2'+data.code);

        }

	});

	$('#claveSelected').bind('change', function(){
		var $this = $(this),
	      $value = $this.val();

		$("#tabla_listaNegra").dataTable().fnDestroy();
		  listaNegraModal($value);
	});


 function graficaListas(arregloserie){

   Highcharts.mapChart('containerGrafica', {
       chart: {
           map: 'countries/mx/mx-all'

       },

       title: {
           text: 'Lista Negra SAT Geográfico'
       },

       subtitle: {
           text: 'Relación SAP-SAT'
       },
       legend: {
            title: {
                text: 'Total de Cuentas Encontradas por Edo.',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }
        },

       mapNavigation: {
           enabled: true,
           buttonOptions: {
               verticalAlign: 'bottom'
           }
       },

       colorAxis: {

          dataClasses: [{
           from: 0,
           to: 5,
           color: '#01DF3A',
           name: 'menos de 5'
       }, {
           from: 5,
           to: 30,
           color: '#FF7',
           name: 'de 5 a 30'
       }, {
           from: 30,
           to: 100,
           color: '#FFA500',
           name: 'de 30 a 100'
       }, {
           from: 100,
           color: '#FF0000',
           name: 'mas de 100'
       }]
      },
      plotOptions: {
       series: {
           cursor: 'pointer',
           events: {
               click: function (event) {

                  $('#EstadoId').html("Lista negra SAT del Estado de "+JSON.stringify(event.point.properties.name));

                  clave_estado=JSON.stringify(event.point.properties.name).replace(/\"/g,'');

                  var clave_edo=null;
                  for (var propiedad in arregloEdo) {
                  if (arregloEdo.hasOwnProperty(propiedad)) {

                    if(clave_estado == propiedad){
                       clave_edo=arregloEdo[propiedad];

                    }

                  }
                }
									$("#tabla_listaNegra").dataTable().fnDestroy();
                  listaNegraModal(clave_edo);
               }
           }
       }
   },

       series: [{
         data: arregloserie,
           name: 'Seleccione para detalle',
           states: {
               hover: {
                   color: '#0000FF'
               }
           },
           dataLabels: {
               enabled: true,
               format: '{point.properties.postal-code}'
           }
       }]
   });

 }


 function listaNegraModal(filtro){

  var formData = {
            'fl_clave_edo': filtro,
        };
  var dataTable = $('#tabla_listaNegra').DataTable({


				 "language":	{
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar:  _MENU_  registros a exportar",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     " Último",
						"sNext":     " Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},

         "lengthMenu": [ [10, 25, 50, 100, 1111], [10, 25, 50, 100,"Todos los registros"] ],

        "scrollY": 250,
        "scrollX": true,
      order: [[ 3, 'desc' ], [ 0, 'asc' ]],
					"processing": false,
					"serverSide": true,

					"ajax":{
						url :"/listaNegraModal", // json datasource
						type: "GET",  // method  , by default get
            data: formData,
            encode: true,
						error: function(){  // error handling
							$(".tabla_listaNegra-error").html("");
							$("#tabla_listaNegra").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos en el servidor</th></tr></tbody>');
							$("#tabla_listaNegra_processing").css("display","none");

						}
					},
          "autoWidth": true

				} );

        var buttons = new $.fn.dataTable.Buttons(dataTable, {

          buttons: [

            {
                extend: 'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o fa-2x"></i>',
                orientation: 'landscape',
                pageSize: 'LEGAL',
								text: 'PDF',
                titleAttr: 'Exportar PDF',
                customize: function ( doc ) {
									var now = new Date();

									var jsDate = now.getFullYear()+'-'+ ("0" + (now.getMonth() + 1)).slice(-2)+'-'+now.getDate();

                	var cols = [];
                	cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                	cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
									doc['footer']=(function(page, pages) {
									return {
										columns: [
											{
												alignment: 'left',
												fontSize: 10,
												text: ['Archivo creado el día : ', { text: jsDate.toString() }]
											},
											{
												alignment: 'right',
												text: ['pagina ', { text: page.toString() },	' de ',	{ text: pages.toString() }]
											}
										],
										margin: 20
									}
								});

								doc['header']=(function() {
									return {
										columns: [
											{
												alignment: 'center',
												fontSize: 14,
												text: 'Filtros Usados'
											 }

										],
										margin: 20
									}
								});

              }
            }, {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o fa-2x"></i>',
                titleAttr: 'Exportar Excel',
								text: 'Excel'
            }

      ]
 }).container().appendTo($('#buttonsTable1'));


  document.getElementById('abcd').style.display = "block";
 }

 $("#exit").on( "click", function() {
			$('#abcd').hide(); //oculto mediante id
			$('.abcd').hide(); //muestro mediante clase
		});

</script>
@endsection
