
@extends('voyager::master')
@section('content')

<style>

  .bg-primary {
      background-color: #19B5FE;
      color : white;
  }

  table.dataTable thead tr {
	  background-color: #82CFFD;
	color : black;
	}

	tfoot tr, thead tr {
		--background: green;
		--color : white;
	}
	tfoot td {
		--font-weight:bold;
	}


	.modal-body {
	  overflow-x: auto;
		width:'auto';
		height:'auto';
		 'max-height':'100%';
	}

	#historico {
	  width:82%;
	  height:100%;
	  opacity:.95;
	  top:0;
	  padding-top: 5%;
	  display:none;
	  position:fixed;
	  background-color:#ffffff;
	  overflow:auto
	}

	form {
	  max-width:100%;
	  min-width:100%;
		--padding:2% 2%;

	  font-family:raleway;
	  background-color:#fff
	}

	p {
	  margin-top:2%
	}

	th { font-size: 12px; }
	td { font-size: 11px; }

  div.container {
	        width: 100%;
	    }

</style>
    <form action="#" id="form" method="post" name="form">
       <div>
					<h2 class="title" align="center" name="Titulo" id="EstadoId">Histórico Lista Negra SAT</h2>
          <button type="button" class="btn btn-danger pull-right"  id="menuFiscal" name="menuFiscal" onclick="retornaMenu();">Regresar Menú</button>
							 <div class="col-md-3" id="cmbEstado" name = "cmbEstado">
								 <div class="input-group">
									<span class="input-group-addon bg-primary ">Estado: </span>
										<div class="input-group">

											<select class="form-control" id="claveSelected2">
												<option value="0">Buscar por Estado</option>
												<option value="AGS">Aguascalientes	</option>
												<option value="BC">Baja California	</option>
												<option value="BCS">Baja California Sur	</option>
												<option value="CDM">Ciudad de México	</option>
												<option value="CHI">Chihuahua	</option>
												<option value="CHS">Chiapas	</option>
												<option value="CMP">Campeche	</option>
												<option value="COA">Coahuila	</option>
												<option value="COL">Colima	</option>
												<option value="DF">Distrito Federal	</option>
												<option value="DGO">Durango	</option>
												<option value="GRO">Guerrero	</option>
												<option value="GTO">Guanajuato	</option>
												<option value="HGO">Hidalgo	</option>
												<option value="JAL">Jalisco	</option>
												<option value="MCH">Michoacán	</option>
												<option value="MEX">Estado de México	</option>
												<option value="MOR">Morelos	</option>
												<option value="NAY">Nayarit	</option>
												<option value="NL">Nuevo León	</option>
												<option value="OAX">Oaxaca	</option>
												<option value="PUE">Puebla	</option>
												<option value="QR">Quintana Roo	</option>
												<option value="QRO">Querétaro	</option>
												<option value="SIN">Sinaloa	</option>
												<option value="SLP">San Luis Potosí	</option>
												<option value="SON">Sonora	</option>
												<option value="TAB">Tabasco	</option>
												<option value="TLX">Tlaxcala	</option>
												<option value="TMS">Tamaulipas	</option>
												<option value="VER">Veracruz	</option>
												<option value="YUC">Yucatán	</option>
												<option value="ZAC">Zacatecas	</option>
												<option value="todos">Todos</option>
										  </select>
											<span class="input-group-addon add-on bg-primary">
												<i class="glyphicon glyphicon-search "></i>
											</span>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											<div class="help-block with-errors"></div>
										</div>
									</div>
							 </div>
              <row>
                <div class="pull-right" >
                   <div class="btn-group">
                    <div id="buttonsTable"></div>
                      </div>
               </div>
             </row>
            <div>
              <div>
              <!--  <table id='tabla_listaNegraHistorica' style="position: relative; overflow: auto; width: 100%;" class='display table table-striped table-bordered table-hover table-condensed '>-->
               <table id='tabla_listaNegraHistorica' class='display table-striped table-bordered table-hover table-condensed ' style="width:100%">
								<!--<table id='tabla_listaNegraHistorica' class='display table table-striped table-bordered table-hover table-condensed '>-->
                  <thead>
                    <tr>
                      <th>Total Eventos en SAT</th>
											<th>RFC</th>
                      <th>Estado Lista Negra SAP</th>
										<!--	<th>Clave Estado</th>-->
											<th>Fecha Estado SAT</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
    </div>
				<!-- Modal -->
			<div id="abc" class="modal fade" tabindex="-1" >
        <div class="modal-dialog" role="document" >
			    <!-- Modal content-->
			    <div class="modal-content" >
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title" align="center" name="rfcId" id="rfcId"></h4>
			      </div>

		 			      <div class="modal-body">
									<div align="right" style="padding-right:10px;">
										 <div class="btn-group">
											<div id="buttonsTable2"></div>
												</div>
								 </div>
		 									<!--<table id='tbl_listaNegraDetHistorico' style="position: relative; overflow: auto; width: 100%;" class='display table-nowrap table-bordered table-hover table-condensed '>-->
											<table id='tbl_listaNegraDetHistorico' style="width:100%" class='display responsive nowrap'>
		 										<thead>
		 											<tr>
		 												<th>Fecha Estado</th>
														<th>Fecha Último Movimiento SAP</th>
		 												<th>Clave Estado</th>
		 											</tr>
		 										</thead>
		 										<tbody>
		 										</tbody>
		 									</table>
		 						    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
					</div>
				</div>

			</div>
		</div>
	</form>
<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>

<script>

function retornaMenu(){
  window.location.href = "{{ route('menuFiscal') }}";
}

$( document ).ready(function() {
    listaNegraHistorica('todos');
  });

$('#claveSelected2').bind('change', function(){
	var $this = $(this),
      $value = $this.val();


$("#tabla_listaNegraHistorica").dataTable().fnDestroy();
  listaNegraHistorica($value);

});

 function listaNegraHistorica(filtro){


   var formData = {
             'fl_clave_edo': filtro,
         };

  var dataTable = $('#tabla_listaNegraHistorica').DataTable({

		     "createdRow": function( row, data, dataIndex){
                if( data[0] !=  "1"){

										$('td', row).css('background-color', '#F0E4DC');
                }
            },


				 "language":	{
					"sProcessing":     'Procesando información por favor espere...',
          //"Processing":     '<span style="width:100%;"><img src="http://www.snacklocal.com/images/ajaxload.gif"></span>',
					"sLengthMenu":     "Mostrar _MENU_ registros a exportar",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					//"sLoadingRecords": '<span style="width:100%;"><img src="http://www.snacklocal.com/images/ajaxload.gif"></span>',
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},
         "lengthMenu": [ [10, 25, 50, 100, 1111], [10, 25, 50, 100,"Todos los registros"] ],

        "scrollY": 250,
        "scrollX": true,
      order: [[ 1, 'desc' ]],
					"processing": true,
          "responsive": true,
					"serverSide": true,
					"ajax":{
						url :"/listaNegraHistorica", // json datasource
						type: "GET",  // method  , by default get
            data: formData,
            encode: true,
						error: function(){  // error handling
							$(".tabla_listaNegraHistorica-error").html("");

							$("#tabla_listaNegraHistorica_processing").css("display","none");

						}
					},
          "autoWidth": true

				} );

				$('#tabla_listaNegraHistorica').on('dblclick', 'tr', function () {
					var data = dataTable.row( this ).data();
          $("#tbl_listaNegraDetHistorico").dataTable().fnDestroy();
          $('#rfcId').html("Detalle Individual por RFC : "+data[1]);
          listaNegraModalHistorica(data[1]);
				} );

/*
        var buttons = new $.fn.dataTable.Buttons(dataTable, {

          buttons: [

            {
                extend: 'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                orientation: 'portrait',
                pageSize: 'LEGAL',
                titleAttr: 'Exportar PDF',
                customize: function ( doc ) {

									var now = new Date();

										var jsDate = now.getFullYear()+'-'+ ("0" + (now.getMonth() + 1)).slice(-2)+'-'+now.getDate();

                var cols = [];
                cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };

																doc['footer']=(function(page, pages) {
																	return {
																		columns: [
																			{
																				alignment: 'left',
																				fontSize: 10,
																				text: ['Archivo creado el día : ', { text: jsDate.toString() }]
																			},
																			{
																				alignment: 'right',
																				text: ['pagina ', { text: page.toString() },	' de ',	{ text: pages.toString() }]
																			}
																		],
																		margin: 20
																	}
																});

																doc['header']=(function() {
																	return {
																		columns: [
																		  {
																				alignment: 'center',
																				fontSize: 14,
																				text: 'Filtros Usados'
																			 }

																		],
																		margin: 20
																	}
																});
                }
            }, {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Exportar Excel'
            }

      ]
 }).container().appendTo($('#buttonsTable'));
*/
 }

  function listaNegraModalHistorica(filtro){


   var formData = {
             'fl_rfc': filtro,
         };

   var dataTable2 = $('#tbl_listaNegraDetHistorico').DataTable({


 				 "language":	{
 					"sProcessing":     "Procesando...",
 					"sLengthMenu":     "Mostrar _MENU_ registros a exportar",
 					"sZeroRecords":    "No se encontraron resultados",
 					"sEmptyTable":     "Ningún dato disponible en esta tabla",
 					"sInfo":           "Reg. _START_ al _END_ de _TOTAL_ registros",
 					"sInfoEmpty":      "Reg. 0 al 0 de 0 registros",
 					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
 					"sInfoPostFix":    "",
 					"sSearch":         "Buscar:",
 					"sUrl":            "",
 					"sInfoThousands":  ",",
 					"sLoadingRecords": "Cargando...",
 					"oPaginate": {
 						"sFirst":    "Primero",
 						"sLast":     "Último",
 						"sNext":     "Siguiente",
 						"sPrevious": "Anterior"
 					},
 					"oAria": {
 						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
 						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
 					}
 				},
          "lengthMenu": [ [10, 25, 50, 100, 1111], [10, 25, 50, 100,"Todos los registros"] ],

         "scrollY": 250,
         "scrollX": true,
       order: [[ 0, 'desc' ]],
 					"processing": false,
 					"serverSide": true,
 					"ajax":{
 						url :"/listaHistoricoDetalle", // json datasource
 						type: "GET",  // method  , by default get
             data: formData,
             encode: true,
 						error: function(){  // error handling
 							$(".tbl_listaNegraDetHistorico-error").html("");

 							$("#tbl_listaNegraDetHistorico_processing").css("display","none");

 						}
 					},
           "autoWidth": true

 				} );
/*
         var buttons = new $.fn.dataTable.Buttons(dataTable2, {

           buttons: [

             {
                 extend: 'pdfHtml5',
                 text:      '<i class="fa fa-file-pdf-o fa-2x"></i>',
                 orientation: 'portrait',
                 pageSize: 'LEGAL',
                 titleAttr: 'Exportar PDF',
                 customize: function ( doc ) {
									 var now = new Date();

									 	var jsDate = now.getFullYear()+'-'+ ("0" + (now.getMonth() + 1)).slice(-2)+'-'+now.getDate();

																		doc['footer']=(function(page, pages) {
																			return {
																				columns: [
																					{
																						alignment: 'left',
																						fontSize: 10,
																						text: ['Archivo creado el día : ', { text: jsDate.toString() }]
																					},
																					{
																						alignment: 'right',
																						text: ['pagina ', { text: page.toString() },	' de ',	{ text: pages.toString() }]
																					}
																				],
																				margin: 20
																			}
																		});

																		doc['header']=(function() {
																			return {
																				columns: [
																				  {
																						alignment: 'center',
																						fontSize: 14,
																						text: 'Filtros Usados'
																					 }

																				],
																				margin: 20
																			}
																		});
                 }
             }, {
                 extend:    'excelHtml5',
                 text:      '<i class="fa fa-file-excel-o fa-2x"></i>',
                 titleAttr: 'Exportar Excel'
             }

       ]
  }).container().appendTo($('#buttonsTable2'));
*/
         $('#abc').modal('show');
				 $('#abc').on('shown.bs.modal', function(e){
				    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
				 });


  }


</script>
@endsection
