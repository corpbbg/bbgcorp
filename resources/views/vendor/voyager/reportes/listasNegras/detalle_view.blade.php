
@extends('voyager::master')

@section('content')

<style>

	.bg-primary {
	    background-color: #19B5FE;
	    color : white;
	}
	table.dataTable thead tr {
	  background-color: #82CFFD;
	color : black;
	}

	tfoot tr, thead tr {
		--background: green;
		--color : white;
	}
	tfoot td {
		--font-weight:bold;
	}


	#abc {
		width:82%;
		height:100%;
		opacity:.95;
		top:0;
		padding-top: 5%;
		display:none;
		position:fixed;
		background-color:#ffffff;
		overflow:auto
	}


	form {
	  max-width:100%;
	  min-width:100%;
		padding:2% 2%;


	  font-family:raleway;
	  background-color:#fff
	}
	p {
	  margin-top:0%
	}


	th { font-size: 12px; }
	td { font-size: 11px; }
	.center {
	    margin: auto;
	    width: 60%;
	    padding: 10px;
	}
	  .loading {
	      margin-top: 10em;
	      text-align: center;
	      color: gray;
	  }

		div.container {
						width: 100%;
				}

</style>
        <div id="abc" class="container-fluid">
          <form action="#" id="form" method="post" name="form">
													<button type="button" class="btn btn-danger pull-right"  id="menuFiscal" name="menuFiscal" onclick="retornaMenu();">Regresar Menú</button>
            <h2 class="modal-title" align="center" name="EstadoId" id="EstadoId">Listado Completo</h2>
						<div class="modal-header">
						<!--<button type="button" class="btn btn-danger pull-right"  id="menuFiscal" name="menuFiscal" ><a href="{ { route('menuFiscal')}}">Regresar Menú</button>-->
						</div>
							 <div class="col-md-3">
								 <div class="input-group">
									<span class="input-group-addon bg-primary">Estado: </span>
										<div class="input-group">

											<select class="form-control" id="claveSelected">
												<option value="0">Buscar por Estado</option>
												<option value="AGS">Aguascalientes	</option>
												<option value="BC">Baja California	</option>
												<option value="BCS">Baja California Sur	</option>
												<option value="CDM">Ciudad de México	</option>
												<option value="CHI">Chihuahua	</option>
												<option value="CHS">Chiapas	</option>
												<option value="CMP">Campeche	</option>
												<option value="COA">Coahuila	</option>
												<option value="COL">Colima	</option>
												<option value="DF">Distrito Federal	</option>
												<option value="DGO">Durango	</option>
												<option value="GRO">Guerrero	</option>
												<option value="GTO">Guanajuato	</option>
												<option value="HGO">Hidalgo	</option>
												<option value="JAL">Jalisco	</option>
												<option value="MCH">Michoacán	</option>
												<option value="MEX">Estado de México	</option>
												<option value="MOR">Morelos	</option>
												<option value="NAY">Nayarit	</option>
												<option value="NL">Nuevo León	</option>
												<option value="OAX">Oaxaca	</option>
												<option value="PUE">Puebla	</option>
												<option value="QR">Quintana Roo	</option>
												<option value="QRO">Querétaro	</option>
												<option value="SIN">Sinaloa	</option>
												<option value="SLP">San Luis Potosí	</option>
												<option value="SON">Sonora	</option>
												<option value="TAB">Tabasco	</option>
												<option value="TLX">Tlaxcala	</option>
												<option value="TMS">Tamaulipas	</option>
												<option value="VER">Veracruz	</option>
												<option value="YUC">Yucatán	</option>
												<option value="ZAC">Zacatecas	</option>
												<option value="todos">Todos</option>
										  </select>
											<span class="input-group-addon add-on bg-primary">
												<i class="glyphicon glyphicon-search"></i>
											</span>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											<div class="help-block with-errors"></div>
										</div>

									</div>


							 </div>

					<br>
              <row>
                <div class="pull-right" >
                   <div class="btn-group">
                    <div id="buttonsTable"></div>
                      </div>
               </div>
             </row>
             <p>
             <row>

             </row>
             <br>
            <div class='box box-primary'>
              <div class='box-body'>
                <table id='tabla_listaNegra' class='display table table-striped table-bordered table-hover table-condensed '>
                  <thead>
                    <tr>
                      <th>Clave SAP</th><th>RFC</th><th>Razón Social</th><th>Sociedad</th><th>Nombre Sociedad</th>
                      <th>Estado Lista Negra SAP</th><th>Estado Lista Negra BBG</th><th>Fecha Último Movimiento SAP</th>
											<th>Clave Estado</th><th>Municipio</th><th>Tipo Relación</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </form>
      </div>

<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>

<script>

function retornaMenu(){

  window.location.href = "{{ route('menuFiscal') }}";
}


$( document ).ready(function() {
    listaNegraModal('todos');
		document.getElementById('abc').style.display = "block";
  });

$('#claveSelected').bind('change', function(){
	var $this = $(this),
      $value = $this.val();


$("#tabla_listaNegra").dataTable().fnDestroy();
  listaNegraModal($value);
});


 function listaNegraModal(filtro){

   var formData = {
             'fl_clave_edo': filtro,
         };

  var dataTable = $('#tabla_listaNegra').DataTable({

				 "language":	{
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ registros a exportar",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},
         "lengthMenu": [ [10, 25, 50, 100, 1111], [10, 25, 50, 100,"Todos los registros"] ],

        "scrollY": 250,
        "scrollX": true,
      order: [[ 3, 'desc' ], [ 0, 'asc' ]],
					"processing": false,
					"serverSide": true,
					"ajax":{
						url :"/listaNegraModal", // json datasource
						type: "GET",  // method  , by default get
            data: formData,
            encode: true,
						error: function(){  // error handling
							$(".tabla_listaNegra-error").html("");
							$('#tabla_listaProcesos').dataTable().clear();
							$("#tabla_listaNegra").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos en el servidor</th></tr></tbody>');
							$("#tabla_listaNegra_processing").css("display","none");

						}
					},
          "autoWidth": true

				} );
/*
        var buttons = new $.fn.dataTable.Buttons(dataTable, {

          buttons: [

            {
                extend: 'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o fa-2x"></i>',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                titleAttr: 'Exportar PDF',
                customize: function ( doc ) {
									var now = new Date();

										var jsDate = now.getFullYear()+'-'+ ("0" + (now.getMonth() + 1)).slice(-2)+'-'+now.getDate();

                var cols = [];
                cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
								doc['footer']=(function(page, pages) {
									return {
										columns: [
											{
												alignment: 'left',
												fontSize: 10,
												text: ['Archivo creado el día : ', { text: jsDate.toString() }]
											},
											{
												alignment: 'right',
												text: ['pagina ', { text: page.toString() },	' de ',	{ text: pages.toString() }]
											}
										],
										margin: 20
									}
								});

								doc['header']=(function() {
									return {
										columns: [
											{
												alignment: 'center',
												fontSize: 14,
												text: 'Filtros Usados'
											 }

										],
										margin: 20
									}
								});
                }
            }, {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o fa-2x"></i>',
                titleAttr: 'Exportar Excel'
            }

      ]
 }).container().appendTo($('#buttonsTable'));
*/
 }

</script>
@endsection
