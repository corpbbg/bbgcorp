
@extends('voyager::master')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@section('content')

<style>
.bg-primary {
		background-color: #19B5FE;
		color : white;
}

table.dataTable thead tr {
	background-color: #82CFFD;
color : black;
}

tfoot tr, thead tr {
	--background: green;
	--color : white;
}
tfoot td {
	--font-weight:bold;
}


div.container {
				width: 100%;
		}
		@media (min-width: 772px) {
	    .modal-xl {
	      width: 90%;
	     max-width:90%;
	    }
	  }
#divTabla {
		width:82%;
				height:100%;
				--opacity:.95;
				top:0;
				padding-top: 5%;
				display:none;
				position:fixed;
				background-color:#ffffff;
				overflow:auto
	}

	img#close {
	  position:absolute;
	  right:-14px;
	  top:-14px;
	  cursor:pointer
	}

	form {
	  max-width:100%;
	  min-width:100%;

	  font-family:raleway;
	  background-color:#fff
	}
p {
		margin-top:0%
	}


	th { font-size: 12px; }
	td { font-size: 11px; }
	/*.center {
	    margin: auto;
	    width: 60%;
	    padding: 10px;
	}
  .loading {
      margin-top: 10em;
      text-align: center;
      color: gray;
  }
*/
	#resultado {
    background-color: red;
    color: white;
    font-weight: bold;
	}
	#resultado.ok {
	    background-color: white;
	}
</style>

          <form action="#" id="form" method="post" name="form">


 						<div id="divTabla" class="container-fluid">
							<h2 class="modal-title" align="center" name="EstadoId" id="EstadoId">Verificación RFC en SAT</h2>
							<button type="button" class="btn btn-danger pull-right"  id="menuFiscal" name="menuFiscal" onclick="retornaMenu();">Regresar Menú</button>
							 <div class="col-md-3">
								 <div class="input-group">
									<span class="input-group-addon bg-primary">RFC: </span>
										<div class="input-group">
											<input type="text" id="rfc_input" style="width:100%;"
										       oninput="validarInput(this)"
										       placeholder="Ingrese su RFC">
										<pre id="resultado"></pre>
											<span id='spamsearch' class="input-group-addon add-on bg-primary">
												<i id="btnSearch" class="glyphicon glyphicon-search"></i>
											</span>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											<div class="help-block with-errors"></div>
										</div>
									</div>
							 </div>
							 	<br>
								<br>
                <table id='tabla_listaSAT'  class='display table-striped table-bordered table-hover table-condensed ' style="width:100%">
                  <thead>
                    <tr>
                      <th>RFC</th><th>Estatus SAT</th><th>Estatus BBG</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
							</div>
            </form>

<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')

<script>
function retornaMenu(){

  window.location.href = "{{ route('menuFiscal') }}";
}


$( document ).ready(function() {
     $("#spamsearch").hide();
		 resultado.classList.add("ok");
		 document.getElementById('divTabla').style.display = "block";
  });


	$('#btnSearch').bind('click', function(){
		var bla = $('#rfc_input').val();
		//	if(''!=bla){

				$("#tabla_listaSAT").dataTable().fnDestroy();

			  listaNegraSAT(bla);
			//}else{
			//		swal('Introduzca un RFC en la caja de texto para continuar!');
		//	}

	});

 function listaNegraSAT(filtro){

   var formData = {
             'fl_rfc':filtro,
         };

  var dataTable = $('#tabla_listaSAT').DataTable({

				 "language":	{
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ registros a exportar",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},
         "lengthMenu": [ [10, 25, 50, 100, 1111], [10, 25, 50, 100,"Todos los registros"] ],

        "scrollY": 250,
        "scrollX": true,
         order: [[ 1, 'desc' ], [ 0, 'asc' ]],
					"processing": false,
					"serverSide": true,
					"ajax":{
						url :"/listadoSAT", // json datasource
						type: "GET",  // method  , by default get
            data: formData,
            encode: true,
					error: function(msg){
        //alert( "Data Saved: " + msg );
							swal('RFC Verificado!', 'El RFC '+filtro+' no se encuentra en la Lista Negra del SAT', 'success');
					}
						/* error: function(jqXHR, textStatus, errorThrown) {


                $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            },


            success: function(data, textStatus, jqXHR) {
                $('#result').html(data);
                alert('Load was performed. Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information! ');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('data:');
                console.log(data);
            }*/
					},
          "autoWidth": true

				} );


      /*  var buttons = new $.fn.dataTable.Buttons(dataTable, {

          buttons: [

            {
                extend: 'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o fa-2x"></i>',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                titleAttr: 'Exportar PDF',
                customize: function ( doc ) {
									var now = new Date();

										var jsDate = now.getFullYear()+'-'+ ("0" + (now.getMonth() + 1)).slice(-2)+'-'+now.getDate();

                var cols = [];
                cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
								doc['footer']=(function(page, pages) {
									return {
										columns: [
											{
												alignment: 'left',
												fontSize: 10,
												text: ['Archivo creado el día : ', { text: jsDate.toString() }]
											},
											{
												alignment: 'right',
												text: ['pagina ', { text: page.toString() },	' de ',	{ text: pages.toString() }]
											}
										],
										margin: 20
									}
								});

								doc['header']=(function() {
									return {
										columns: [
											{
												alignment: 'center',
												fontSize: 14,
												text: 'Filtros Usados'
											 }

										],
										margin: 20
									}
								});
                }
            }, {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o fa-2x"></i>',
                titleAttr: 'Exportar Excel'
            }

      ]
 }).container().appendTo($('#buttonsTable'));
*/


 }
 function rfcValido(rfc, aceptarGenerico = true) {
     const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
     var   validado = rfc.match(re);
		 var bandera = null;
     if (!validado)  //Coincide con el formato general del regex?
         return false;

     //Separar el dígito verificador del resto del RFC
     const digitoVerificador = validado.pop(),
           rfcSinDigito      = validado.slice(1).join(''),
           len               = rfcSinDigito.length,

     //Obtener el digito esperado
           diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
           indice            = len + 1;
     var   suma,
           digitoEsperado;

     if (len == 12) suma = 0
     else suma = 481; //Ajuste para persona moral

     for(var i=0; i<len; i++)
         suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
     digitoEsperado = 11 - suma % 11;
     if (digitoEsperado == 11) digitoEsperado = 0;
     else if (digitoEsperado == 10) digitoEsperado = "A";

     //El dígito verificador coincide con el esperado?
     // o es un RFC Genérico (ventas a público general)?
     if ((digitoVerificador != digitoEsperado)
      && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
         return false;
     else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
         return false;
     return rfcSinDigito + digitoVerificador;
 }


 //Handler para el evento cuando cambia el input
 // -Lleva la RFC a mayúsculas para validarlo
 // -Elimina los espacios que pueda tener antes o después
 function validarInput(input) {
     var rfc         = input.value.trim().toUpperCase(),
         resultado   = document.getElementById("resultado"),
         valido;

     var rfcCorrecto = rfcValido(rfc);   // ⬅️ Acá se comprueba

     if (rfcCorrecto) {
    // 	valido = "Válido para buscar en SAT";
		//	bandera = true;
       resultado.classList.add("ok");
        $("#spamsearch").show();
     } else {

     	valido = "No válido para buscarlo en SAT";
		//	bandera = false;
				 $("#spamsearch").hide();
     	resultado.classList.remove("ok");
     }

     resultado.innerText = "Formato RFC " + valido;
												 if(bandera){
													  $("#spamsearch").show();
												 }else{
													 $("#spamsearch").hide();
												 }
 }

</script>
@endsection
