@extends('voyager::master')
@section('content')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <style>
  /* Makes images fully responsive */

  .img-responsive,
  .thumbnail > img,
  .thumbnail a > img,
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    display: block;
    width: 100%;
    height: auto;
  }

  /* ------------------- Carousel Styling ------------------- */

  .carousel-inner {
    border-radius: 5px;
    height: 250px;
  overflow: hidden;
  }

  .carousel-caption {
    background-color: rgba(0,0,0,.5);
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 10;
    padding: 0 0 10px 25px;
    color: #fff;
    text-align: left;
  }

  .carousel-indicators {
    position: absolute;
    bottom: 0;
    right: 0;
    left: 0;
    width: 100%;
    z-index: 15;
    margin: 0;
    padding: 0 25px 25px 0;
    text-align: right;
  }

  .carousel-control.left,
  .carousel-control.right {
    background-image: none;
  }


  /* ------------------- Section Styling - Not needed for carousel styling ------------------- */

  .section-white {
     padding: 10px 0;
  }

  .section-white {
    background-color: #fff;
    color: #555;
  }

  @media screen and (min-width: 387px) {

    .section-white {
       padding: 1.5em 0;
    }

  }

  @media screen and (min-width: 992px) {

    .container {
      max-width: 930px;
    }

  }

  </style>

<section class="section-white">
  <div class="container">

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\geografico.jpg" alt="...">
          <div class="carousel-caption">
            <h2>Reportes Geográficos</h2>
            <p>Abrir Reporte!</p>
          </div>
        </div>
        <div class="item">
          <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\historico.jpg" alt="...">
          <div class="carousel-caption">
            <h2>Reportes Históricos</h2>
            <p>Abrir Reporte!</p>
          </div>
        </div>
        <div class="item">
          <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\detalle.jpg" alt="...">
          <div class="carousel-caption">
            <h2>Detalle Listas SAT</h2>
            <p>Abrir Reporte!</p>
          </div>
        </div>
        <div class="item">
          <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="...">
          <div class="carousel-caption">
            <h2>Lista Negra SAT</h2>
            <p>Abrir Reporte!</p>
          </div>
        </div>
      </div>
      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
      <p>
      <div class="row">
	      <div class="col-md-4">
	        <div class="thumbnail">
	          <a href="{{ route('listaNegra') }}">
	            <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\geografico.jpg" alt="Lights" style="width:30%">
	            <div class="caption">
	              <p>Muestra el reporte de Listas Negras SAT Geográfico.</p>
	            </div>
	          </a>
	        </div>
	      </div>
	      <div class="col-md-4">
	        <div class="thumbnail">
	            <a href="{{ route('listaHistorico') }}">
	            <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\historico.jpg" alt="Nature" style="width:30%">
	            <div class="caption">
	              <p>Detalle Histórico de Listas Negras SAT</p>
	            </div>
	          </a>
	        </div>
	      </div>
	      <div class="col-md-4">
	        <div class="thumbnail">
	          <a href="{{ route('listaDetalle') }}">
	            <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\detalle.jpg" alt="Fjords" style="width:30%">
	            <div class="caption">
	              <p>Muestra el detalle de clientes y proveedores que tienen relación en SAP.</p>
	            </div>
	          </a>
	        </div>
	      </div>
    </div>
    <div class="row">
	    <div class="col-md-4">
      <div class="thumbnail">
          <a href="{{ route('listaSAT') }}">
          <img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="Lights" style="width:30%">
          <div class="caption">
            <p>Verificación RFC en Listas Negras del SAT.</p>
          </div>
        </a>
      </div>
    </div>
		<div class="col-md-4">
			<div class="thumbnail">
					<a href="{{ route('listaProcConfig') }}">
					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\mejorarprocesos.jpg" alt="Lights" style="width:28%">
					<div class="caption">
						<p>Mantenimiento de catalogos Procesos Configuraciones.</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="thumbnail">
					<a href="{{ route('cuentaMayor') }}">
					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\mejorarprocesos.jpg" alt="Lights" style="width:28%">
					<div class="caption">
						<p>Mantenimiento de Cuentas de Mayor.</p>
					</div>
				</a>
			</div>
		</div>
		<!--<div class="col-md-4">
			<div class="thumbnail">
					<a href="{ { route('listaProcesos') }}">
					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="Lights" style="width:30%">
					<div class="caption">
						<p>Mantenimiento de catalogos procesos.</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="thumbnail">
					<a href="{ { route('listaParametros') }}">
					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="Lights" style="width:30%">
					<div class="caption">
						<p>Mantenimiento de catalogos parametros.</p>
					</div>
				</a>
			</div>
		</div>-->
  	<!--</div>
	<div class="row">
	<div class="col-md-4">
			<div class="thumbnail">
					<a href="{ { route('listaProcUsuarios') }}">
					<img src="..\vendor\tcg\voyager\assets\images\widget-backgrounds\listaNegra.jpeg" alt="Lights" style="width:30%">
					<div class="caption">
						<p>Mantenimiento de catalogos Procesos por Usuario.</p>
					</div>
				</a>
			</div>
		</div>

-->
	   </div>
    </div>

</div>
</section>

@endsection
