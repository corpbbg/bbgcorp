
<?php
$cons_usuario="WEBUSER";
$cons_contra="W2b88gC4rp";
$cons_base_datos="DWH_Interfaces";
$cons_equipo="10.0.101.10";

$serverName = "$cons_equipo\sqlexpress, 1433"; //serverName\instanceName, portNumber (por defecto es 1433)

$connectionInfo = array( "Database"=>$cons_base_datos, "UID"=>$cons_usuario, "PWD"=>$cons_contra,"CharacterSet" => "UTF-8");
$obj_conexion = sqlsrv_connect( $serverName, $connectionInfo);

$usuario = strtoupper(strtok(Auth::user()->email, "@"));

$sqlFiltros="";

$clauses=array();

    if( isset( $_GET['fl_clave'] ) && !empty( $_GET['fl_clave'] ) ){
        $clauses[] = " CLAVEPARAMETRO = '{$_GET['fl_clave']}'";
    }
    //if ( isset( $_GET['min_price'], $_GET['max_price'] ) && !empty( $_GET['min_price'] )  && !empty( $_GET['max_price'] ) ) {
    //    $clauses[]="`sup_price` >= '{$_GET['min_price']}'";
    //    $clauses[]="`sup_price` <= '{$_GET['max_price']}'";
    //}
    if ( isset( $_GET['id_proceso'] ) && !empty( $_GET['id_proceso'] ) ){
        $clauses[]=" CLAVEPROCESO like '%{$_GET['id_proceso']}%'";
    }


      $sqlFiltros = !empty( $clauses ) ? ' and '.implode(' and ',$clauses ) : '';


$clave = "";
if($_GET["accion"] == "list")
  	{

        $consulta= "SELECT * FROM (SELECT *,ROW_NUMBER() OVER (ORDER BY CLAVECONFIG) AS RowNumber FROM VW_listaCatalogoConfig WHERE USUARIO = '$usuario' "." $sqlFiltros".") f WHERE RowNumber > ". $_GET['jtStartIndex'] ." AND RowNumber <= ". $_GET['jtPageSize'] ." ";


        $consulta2="Select COUNT(*) AS RecordCount FROM VW_listaCatalogoConfig WHERE USUARIO = '$usuario' ". $sqlFiltros;
//echo $consulta2;
        $stmt=sqlsrv_query($obj_conexion, $consulta2);
        $row = sqlsrv_fetch_array($stmt);
        $recordCount = $row['RecordCount'];

        if($resultado = $stmt2=sqlsrv_query($obj_conexion, $consulta)){

          $rows=array();
          while($row=sqlsrv_fetch_array($stmt2)){

            $rows[]=$row;
          }

        }

        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $rows;

        if( $stmt && $stmt2 ) {
             sqlsrv_commit( $obj_conexion );

        } else {
             sqlsrv_rollback( $obj_conexion );

        }

        sqlsrv_free_stmt($stmt);
        sqlsrv_free_stmt($stmt2);

  }

if($_GET["accion"] == "update"){

		$consulta = "UPDATE SAPQAL.QAL.qal.ZTBL_PROCCONFIG SET VALOR  = ?, VALOR2  = ?, VALOR3  = ?, CLAVEPROCESO = ?, CLAVEPARAMETRO  = ? WHERE CLAVECONFIG = ? ";

    $params = array($_GET["VALOR"],$_GET["VALOR2"],$_GET["VALOR3"],$_GET["CLAVEPROCESO"],$_GET["CLAVEPARAMETRO"],$_GET["CLAVECONFIG"]);

    $stmt = sqlsrv_query( $obj_conexion, $consulta, $params);

		$jTableResult = array();
		$jTableResult['Result'] = "OK";

    if( $stmt  ) {
         sqlsrv_commit( $obj_conexion );

    } else {
         sqlsrv_rollback( $obj_conexion );

    }

    sqlsrv_free_stmt($stmt);

}
if($_GET["accion"] == "delete"){

  		$consulta = "DELETE FROM SAPQAL.QAL.qal.ZTBL_PROCCONFIG WHERE CLAVECONFIG = ? ";
      $params = array($_GET["CLAVECONFIG"]);

      $stmt = sqlsrv_query( $obj_conexion, $consulta, $params);

  		$jTableResult = array();
  		$jTableResult['Result'] = "OK";

      if( $stmt ) {
           sqlsrv_commit( $obj_conexion );
      } else {
           sqlsrv_rollback( $obj_conexion );
      }

      sqlsrv_free_stmt($stmt);

}
if($_GET["accion"] == "create"){
    $params2 = array(200,$_GET["CLAVEPROCESO"],$_GET["CLAVEPARAMETRO"],$_GET["VALOR"],$_GET["VALOR2"],$_GET["VALOR3"]);
    $consulta = "SELECT COUNT(*) AS RecordCount from SAPQAL.QAL.qal.ZTBL_PROCCONFIG WHERE MANDT = ? AND CLAVEPROCESO = ? AND CLAVEPARAMETRO =? AND VALOR =? AND VALOR2 =? AND VALOR3 =? ";

    $stmt = sqlsrv_query( $obj_conexion, $consulta, $params2);

    $row = sqlsrv_fetch_array($stmt);
    $i=0;
    $n = $row['RecordCount'];

    if($n>$i){

      $jTableResult = array();
      $jTableResult['Result'] = "ERR";
      $jTableResult['Message'] = "Proceso ya establecido";

    }else{

      $consulta = "SELECT count(*) as RecordCount from SAPQAL.QAL.qal.ZTBL_PROCCONFIG ";

      $stmt = sqlsrv_query( $obj_conexion, $consulta);

      $row = sqlsrv_fetch_array($stmt);
      $i=0;
      $n2 = $row['RecordCount'];

      if($n2 == $i){
        $params = array($row['CLAVEPROCESO'],$_GET["CLAVEPARAMETRO"],$_GET["VALOR"],$_GET["VALOR2"],$_GET["VALOR3"]);
        $consulta2 = "INSERT INTO SAPQAL.QAL.qal.ZTBL_PROCCONFIG(MANDT, CLAVEPROCESO, CLAVEPARAMETRO, CLAVECONFIG,VALOR,VALOR2,VALOR3) VALUES(200,?,1,?,?,?,?)";

      }else{

        $consulta = "SELECT top 1 *  FROM SAPQAL.QAL.qal.ZTBL_PROCCONFIG order by cast(claveconfig as int) desc";
        $stmt = sqlsrv_query( $obj_conexion, $consulta);
    		$row = sqlsrv_fetch_array($stmt);

        $params = array(200,$_GET["CLAVEPROCESO"],$_GET["CLAVEPARAMETRO"],$_GET["VALOR"],$_GET["VALOR2"],$_GET["VALOR3"],$row["CLAVECONFIG"]+1);

        $consulta2 = "INSERT INTO SAPQAL.QAL.qal.ZTBL_PROCCONFIG(MANDT, CLAVEPROCESO, CLAVEPARAMETRO, VALOR, VALOR2, VALOR3, CLAVECONFIG) VALUES(?,?,?,?,?,?,?)";
      }

        $stmt2 = sqlsrv_query( $obj_conexion, $consulta2, $params);

        $consulta = "SELECT top 1 *  FROM SAPQAL.QAL.qal.ZTBL_PROCCONFIG order by cast(claveconfig as int) desc";

        $stmt2 = sqlsrv_query( $obj_conexion, $consulta);
        $row = sqlsrv_fetch_array($stmt2);

        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['Record'] = $row;

        if( $stmt && $stmt2 ) {
             sqlsrv_commit( $obj_conexion );

        } else {
             sqlsrv_rollback( $obj_conexion );

        }

            sqlsrv_free_stmt($stmt);
            sqlsrv_free_stmt($stmt2);

    }

}

  print json_encode($jTableResult);

  sqlsrv_close($obj_conexion);

?>
