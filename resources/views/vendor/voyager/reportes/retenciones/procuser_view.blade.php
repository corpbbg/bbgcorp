@extends('voyager::master')

@section('content')

<style>

	.bg-primary {
			background-color: #19B5FE;
			color : white;
	}

	table.dataTable thead tr {
	  background-color: #82CFFD;
	color : black;
	}

	div.container {
	        width: 100%;
	    }

	form {
	  max-width:100%;
	  min-width:100%;

	  font-family:raleway;
	  background-color:#fff
	}
	p {
	  margin-top:0%
	}

	th { font-size: 12px; }
	td { font-size: 11px; }

  jtable-input-readonly{
    background-color:lightgray;
  }

</style>
	<form action="#" id="form" method="post" name="form">
<h2 class="modal-title" align="center" style="padding-top:2%" name="EstadoId" id="EstadoId">Catalogo de Configuración de procesos por Usuario</h2>

	  <div class="row">
      <div class="modal-header">
        <div class="btn-group pull-right" >
           <button type="button" class="btn btn-primary " data-dismiss="modal" id="exit" name="exit" >Regresar Listado Configuraciones</button>
           <button type="button" class="btn btn-danger " id="menu" name="menu" onclick="">Regresar Menú</button>

        </div>

			</div>

    </div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content" style=" padding-left:2%">

           <div class='col-md-6'>

               <div class="input-group" >
                 <input type='text' class='form-control' id='claveParametro' placeholder='Escribe la clave del proceso usuario'>
                    <div class="input-group-btn" >
                      <button class="btn btn-info" type='button' id="CargarRegistros">
                          <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
               </div>

           </div>

           <br>

            <div class='col-md-12' >
              <div id='tabla_procesoUsuario' class="table-responsive"></div>
            </div>


      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <div class="control-sidebar-bg"></div>
	</form>

	<?php
					 $cons_usuario="WEBUSER";
					 $cons_contra="W2b88gC4rp";
					 $cons_base_datos="DWH_Interfaces";
					 $cons_equipo="10.0.101.10";

					 $serverName = "$cons_equipo\sqlexpress, 1433"; //serverName\instanceName, portNumber (por defecto es 1433)

					 $connectionInfo = array( "Database"=>$cons_base_datos, "UID"=>$cons_usuario, "PWD"=>$cons_contra,"CharacterSet" => "UTF-8");
					 $obj_conexion = sqlsrv_connect( $serverName, $connectionInfo);
		?>

  <!-- REQUIRED JS SCRIPTS -->
  @yield('js')
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

      	<script type="text/javascript">
             var listaProcesos = [];
      	    $( document ).ready(function() {

              var $dfd = $.Deferred();
                   $('#tabla_procesoUsuario').jtable({
                                title: 'Catalogo de Procesos por Usuario',
                                paging: true,
                                sorting: true,
      				                  pageSize: 30,
                                pageSizeChangeArea: false,
                                defaultSorting: 'CLAVEPARAMETRO ASC',
                                gotoPageArea: 'none',
      						              useBootstrap: true,
                                ajaxSettings: {
                                    type: 'GET'
                                },
                                actions: {
                                         listAction: '/crud_procusuario?accion=list',
                                         updateAction: '/crud_procusuario?accion=update',
                                         deleteAction:'/crud_procusuario?accion=delete',
                                         createAction:'/crud_procusuario?accion=create',
                                },
                                fields: {
																				CONSECUTIVO: {
																								 title: 'Id Parametro ',
																								 width: '0%',
																			           create: true,
																								 key: true,
				                                         list: true,
																								 inputClass: 'validate[required]',
																								 input: function (data) {
				                                              if (data.value) {
				                                                  return '<input type="text" readonly class="jtable-input-readonly" name="CONSECUTIVO" value="' + data.value + '"/>';
				                                              } else {
				                                                  return '<input type="text" readonly class="jtable-input-readonly" name="CONSECUTIVO" value="Autogenerado"/>';
				                                              }
				                                          }
																				},
                                       CLAVEPROCESO: {
                                         title: 'Clave Proceso',
                                         width: '20%',
                                         //key: true,
                                         list: true,
                                         create: true,
                                         edit: true,
                                         options:<?php

                        												  $consulta="SELECT * FROM SAPQAL.DEV.dev.ZTBL_PROCESOS order by CLAVEPROCESO";
                                                  $consulta2="Select COUNT(*) AS RecordCount from SAPQAL.DEV.dev.ZTBL_PROCESOS";
                                                  $stmt=sqlsrv_query($obj_conexion, $consulta2);
                                                  $row1 = sqlsrv_fetch_array($stmt);
                                                  $n = $row1['RecordCount'];

                        												  $stmt2=sqlsrv_query($obj_conexion, $consulta);

                        												  echo "{";
                            												$i=0;

                            												while($row = sqlsrv_fetch_array($stmt2)){
                            													$i++;
                            													echo "'".$row['CLAVEPROCESO']."':'".$row['DESCRIPCION']."'";

                            													if($i<$n){
                            														echo ",";
                            													}
                            												}
                          												echo "}";

																								?>

                                        },
                                        CLAVEPARAMETRO: {
                                          title: 'Clave Parametro',
                                          width: '20%',
                                          //key: true,
                                          list: true,
                                          create: true,
																					edit: true,
                                          options:<?php
																									 sqlsrv_free_stmt($stmt);
																									 sqlsrv_free_stmt($stmt2);
                         												   $consulta="SELECT * FROM SAPQAL.DEV.dev.ZTBL_PARAMPROC order by CLAVEPROCESO";
                                                   $consulta2="Select COUNT(*) AS RecordCount from SAPQAL.DEV.dev.ZTBL_PARAMPROC";
                                                   $stmt2=sqlsrv_query($obj_conexion, $consulta2);
                                                   $row1 = sqlsrv_fetch_array($stmt2);
                                                   $n = $row1['RecordCount'];

                         												  $stmt=sqlsrv_query($obj_conexion, $consulta);

                         												  echo "{";
                             												$i=0;

                             												while($row = sqlsrv_fetch_array($stmt)){
                             													$i++;
                             													echo "'".$row['CLAVEPARAMETRO']."':'".$row['DESCRIPCION']."'";

                             													if($i<$n){
                             														echo ",";
                             													}
                             												}
                           												echo "}";

 																								?>

                                         },
                                        USUARIO: {
                                                 title: 'Nombre del Usuario ',
                                                 width: '60%',
                                                 list: true,
                                                 create: true,
                                                 edit: true,
              								                   inputClass: 'validate[required]',
																								 options:<?php

																													 $consulta="SELECT UNAME as USUARIO FROM sapprod.PRO.pro.AGR_USERS WHERE MANDT = '500' AND TO_DAT >= CONVERT(VARCHAR, GETDATE(), 112) GROUP BY UNAME order by UNAME";
																													$consulta2="Select COUNT(*) AS RecordCount FROM sapprod.PRO.pro.AGR_USERS WHERE MANDT = '500' AND TO_DAT >= CONVERT(VARCHAR, GETDATE(), 112)";
																													$stmt2=sqlsrv_query($obj_conexion, $consulta2);
																													$row1 = sqlsrv_fetch_array($stmt2);
																													$n = $row1['RecordCount'];

																													$stmt=sqlsrv_query($obj_conexion, $consulta);

																													echo "{";
																														$i=0;

																														while($row = sqlsrv_fetch_array($stmt)){
																															$i++;
																															echo "'".$row['USUARIO']."':'".$row['USUARIO']."'";

																															if($i<$n){
																																echo ",";
																															}
																														}
																													echo "}";

																												?>


                                        }

              									}


                    });

                   $('#CargarRegistros').click(function (e){
              				e.preventDefault();
                      $('#tabla_procesoUsuario').jtable({ajaxSettings: { type: 'GET', cache:false }});

              				$('#tabla_procesoUsuario').jtable('load', {
              					fl_clave: $('#claveParametro').val()
              				});
            			});

      			      $('#CargarRegistros').click();

          });
    </script>
<?php
	sqlsrv_free_stmt($stmt);
	sqlsrv_free_stmt($stmt2);
	sqlsrv_close($obj_conexion);
?>
@endsection
