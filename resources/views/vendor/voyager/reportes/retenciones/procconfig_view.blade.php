@extends('voyager::master')

@section('content')

<style>

	.bg-primary {
			background-color: #19B5FE;
			color : white;
	}

	table.dataTable thead tr {
	  background-color: #82CFFD;
	color : black;
	}

	div.container {
	        width: 100%;
	    }

	form {
	  max-width:100%;
	  min-width:100%;

	  font-family:raleway;
	  background-color:#fff
	}
	p {
	  margin-top:0%
	}

	th { font-size: 12px; }
	td { font-size: 11px; }

	jtable-input-readonly{
		background-color:lightgray;
	}


</style>

	<form action="#" id="form" method="post" name="form">
		<h2 class="modal-title" align="center" style="padding-top:2%" name="EstadoId" id="EstadoId">Catalogo Configuración de Procesos</h2>
		<?php

			$usuario = strtoupper(strtok(Auth::user()->email, "@"));
						 $cons_usuario="WEBUSER";
						 $cons_contra="W2b88gC4rp";
						 $cons_base_datos="DWH_Interfaces";
						 $cons_equipo="10.0.101.10";

						 $serverName = "$cons_equipo\sqlexpress, 1433"; //serverName\instanceName, portNumber (por defecto es 1433)

						 $connectionInfo = array( "Database"=>$cons_base_datos, "UID"=>$cons_usuario, "PWD"=>$cons_contra,"CharacterSet" => "UTF-8");
						 $obj_conexion = sqlsrv_connect( $serverName, $connectionInfo);
			?>

	  <div class="row">
      <div class="modal-header">
        <div class="btn-group pull-right" >
           <button type="button" class="btn btn-primary " data-dismiss="modal" id="exit" name="exit" >Regresar Listado Configuraciones</button>
           <button type="button" class="btn btn-danger " id="menu" name="menu" onclick="">Regresar Menú</button>

        </div>

			</div>

    </div>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content" style=" padding-left:2%">

					<div class="row">

							 <div class="col-md-8">
								 <div class='col-md-4'>

			               <div class="input-group" >

											 <label for="idproceso" class="col-sm-4 control-label">Filtrar Proceso</label>
			                 <input type='text' class='form-control' id='idproceso' placeholder='Escribe el proceso a filtrar...'>

			               </div>

			           </div>
								 <div class="col-md-4">
									 <div class='input-group'>

										 <label for="claveParametrofiltro" class="col-sm-4 control-label">Filtrar Parametro:</label>

													 <select name="claveParametro" id="claveParametro" class="form-control">
													 <option value="" selected="selected">Seleccione Parametro</option>
														 <?php
														   $consul="SELECT PP.CLAVEPARAMETRO as clave, P.DESCRIPCION as descProc  ,PP.DESCRIPCION as descParam  FROM SAPQAL.DEV.dev.ZTBL_PROCUSUARIO PU,SAPQAL.DEV.dev.ZTBL_PARAMPROC PP,SAPQAL.DEV.dev.ZTBL_PROCESOS P where USUARIO = '$usuario' and P.CLAVEPROCESO = PP.CLAVEPROCESO GROUP BY PP.CLAVEPARAMETRO, P.DESCRIPCION  ,PP.DESCRIPCION ";
															 echo $consul;
															 $stmt=sqlsrv_query($obj_conexion, $consul);
																 while($rowLineas = sqlsrv_fetch_array($stmt)){
																 echo '<option value="'.$rowLineas['clave'].'">'.$rowLineas['descProc'].' : '.$rowLineas['descParam'].'</option>';
															 }
														 ?>
													 </select>

									 </div>
								 </div>
								 <div class="col-md-4 pull-center" align="center">

										 <!--<label for="" class="col-sm-2 control-label"></label>
										 <a id='CargarRegistros' class="btn btn-primary" >Consultar </a>
										 <label for="" class="col-sm-2 control-label"></label>
										 <a id='limpiar' class="btn btn-primary" onclick="limpiar();">Limpiar </a>-->
										 <div class="btn-group pull-right" >
						            <button type="button" class="btn btn-primary " data-dismiss="modal" id="CargarRegistros" name="exit" >Consultar</button>
						            <button type="button" class="btn btn-warning " id="menu" name="menu" onclick="limpiar();">Limpiar</button>
						         </div>

								 </div>
							 </div>
						 </div>

           <br>

            <div class='col-md-12' >
              <div id='tabla_procesoConfiguracion' class="table-responsive"></div>
            </div>


      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <div class="control-sidebar-bg"></div>
	</form>


  @yield('js')
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

      	<script type="text/javascript">
             var listaProcesos = [];
      	    $( document ).ready(function() {

              var $dfd = $.Deferred();
                   $('#tabla_procesoConfiguracion').jtable({
                                title: 'Tabla Procesos - Configuración',
                                paging: true,
                                sorting: true,
      				                  pageSize: 30,
                                pageSizeChangeArea: false,
                                defaultSorting: 'CLAVECONFIG ASC',
                                gotoPageArea: 'none',
      						              useBootstrap: true,
                                ajaxSettings: {
                                    type: 'GET'
                                },
                                actions: {
                                         listAction: '/crud_procconfig?accion=list',
                                         updateAction: '/crud_procconfig?accion=update',
                                         deleteAction:'/crud_procconfig?accion=delete',
                                         createAction:'/crud_procconfig?accion=create',
                                },
                                fields: {
                                       CLAVEPROCESO: {
                                         title: 'Clave Proceso',
                                         width: '20%',
                                         key: true,
                                         list: true,
                                         create: true,
                                         edit: true,
																				 inputClass: 'validate[required]',
                                         options:<?php

                        												  $consulta="SELECT P.CLAVEPROCESO as CLAVEPROCESO, P.DESCRIPCION as DESCRIPCION   FROM SAPQAL.DEV.dev.ZTBL_PROCUSUARIO PU,SAPQAL.DEV.dev.ZTBL_PROCESOS P where USUARIO = '$usuario'  GROUP BY P.CLAVEPROCESO, P.DESCRIPCION order by P.CLAVEPROCESO";
                                                  $consulta2="SELECT top (1) count(*) as RecordCount FROM SAPQAL.DEV.dev.ZTBL_PROCUSUARIO PU,SAPQAL.DEV.dev.ZTBL_PROCESOS P where USUARIO = '$usuario' ";
                                                  $stmt=sqlsrv_query($obj_conexion, $consulta2);
                                                  $row1 = sqlsrv_fetch_array($stmt);
                                                  $n = $row1['RecordCount'];

                        												  $stmt2=sqlsrv_query($obj_conexion, $consulta);

                        												  echo "{";
                            												$i=0;

                            												while($row = sqlsrv_fetch_array($stmt2)){
                            													$i++;
                            													echo "'".$row['CLAVEPROCESO']."':'".$row['DESCRIPCION']."'";

                            													if($i<$n){
                            														echo ",";
                            													}
                            												}
                          												echo "}";

																								?>

                                        },
                                        CLAVEPARAMETRO: {
                                          title: 'Clave Parametro',
                                          width: '20%',
                                          key: true,
                                          list: true,
                                          create: true,
																					edit: true,
																					inputClass: 'validate[required]',
                                          options:<?php
																									 sqlsrv_free_stmt($stmt);
																									 sqlsrv_free_stmt($stmt2);
                         												   $consulta="SELECT PP.CLAVEPARAMETRO as CLAVEPARAMETRO ,PP.DESCRIPCION as DESCRIPCION  FROM SAPQAL.DEV.dev.ZTBL_PROCUSUARIO PU,SAPQAL.DEV.dev.ZTBL_PARAMPROC PP,SAPQAL.DEV.dev.ZTBL_PROCESOS P where USUARIO = '$usuario'  GROUP BY PP.CLAVEPARAMETRO, PP.DESCRIPCION ";
                                                   $consulta2="SELECT top (1) count(*) as RecordCount FROM SAPQAL.DEV.dev.ZTBL_PROCUSUARIO PU,SAPQAL.DEV.dev.ZTBL_PARAMPROC PP,SAPQAL.DEV.dev.ZTBL_PROCESOS P where USUARIO = '$usuario'";
                                                   $stmt2=sqlsrv_query($obj_conexion, $consulta2);
                                                   $row1 = sqlsrv_fetch_array($stmt2);
                                                   $n = $row1['RecordCount'];

                         												   $stmt=sqlsrv_query($obj_conexion, $consulta);

                         												  echo "{";
                             												$i=0;

                             												while($row = sqlsrv_fetch_array($stmt)){
                             													$i++;
                             													echo "'".$row['CLAVEPARAMETRO']."':'".$row['DESCRIPCION']."'";

                             													if($i<$n){
                             														echo ",";
                             													}
                             												}
                           												echo "}";

 																								?>

                                         },
																				 CLAVECONFIG: {
																									title: 'Clave Configuración',
																									width: '15%',
																									list: true,
																									create: true,
																									key: true,
																									input: function (data) {
				                                               if (data.value) {
				                                                   return '<input type="text" readonly class="jtable-input-readonly" name="JobId" value="' + data.value + '"/>';
				                                               } else {
				                                                   return '<input type="text" readonly class="jtable-input-readonly" name="JobId" value="Autogenerado"/>';
				                                               }
				                                           },
																								 inputClass: 'validate[required]'
																				 },
                                        VALOR: {
                                                 title: 'Configuración 1',
                                                 width: '15%',
                                                 list: true,
                                                 create: true,
                                                 edit: true,
              								                   inputClass: 'validate[required]'
                                        }
																				,
																			 VALOR2: {
																								title: 'Configuración 2',
																								width: '15%',
																								list: true,
																								create: true,
																								edit: true
																								//inputClass: 'validate[required]'
																			 }	,
 																			 VALOR3: {
 																								title: 'Configuración 3',
 																								width: '15%',
 																								list: true,
 																								create: true,
 																								edit: true
 																								//inputClass: 'validate[required]'
 																			 }

              									}

                    });

                   $('#CargarRegistros').click(function (e){

              				e.preventDefault();
                      $('#tabla_procesoConfiguracion').jtable({ajaxSettings: { type: 'GET', cache:false }});

              				$('#tabla_procesoConfiguracion').jtable('load', {
              					fl_clave: $('#claveParametro').val(),
												id_proceso: $('#idproceso').val()
												//usuario: 'miriam'
              				});
            			});

      			      $('#CargarRegistros').click();

          });

					function limpiar(){
						$('#claveParametro').val('');
						$('#idproceso').val('0');


					}
    </script>
<?php
	sqlsrv_free_stmt($stmt);
	sqlsrv_free_stmt($stmt2);
	sqlsrv_close($obj_conexion);
?>
@endsection
