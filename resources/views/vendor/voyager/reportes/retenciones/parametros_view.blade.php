@extends('voyager::master')

@section('content')

<style>

	.bg-primary {
			background-color: #19B5FE;
			color : white;
	}

	table.dataTable thead tr {
	  background-color: #82CFFD;
	color : black;
	}

	div.container {
	        width: 100%;
	    }

	form {
	  max-width:100%;
	  min-width:100%;

	  font-family:raleway;
	  background-color:#fff
	}
	p {
	  margin-top:0%
	}

	th { font-size: 12px; }
	td { font-size: 11px; }

  jtable-input-readonly{
    background-color:lightgray;
  }

</style>
	<form action="#" id="form" method="post" name="form">
	<h2 class="modal-title" align="center" style="padding-top:2%" name="EstadoId" id="EstadoId">Catalogo de Parametros</h2>

	  <div class="row">
      <div class="modal-header">
        <div class="btn-group pull-right" >
           <button type="button" class="btn btn-primary " data-dismiss="modal" id="exit" name="exit" >Regresar Listado Configuraciones</button>
           <button type="button" class="btn btn-danger " id="menu" name="menu" onclick="">Regresar Menú</button>

        </div>

			</div>

    </div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content" style=" padding-left:2%">

           <div class='col-md-6'>

               <div class="input-group" >
                 <input type='text' class='form-control' id='claveParametro' placeholder='Escribe la clave del Parametro'>
                    <div class="input-group-btn" >
                      <button class="btn btn-info" type='button' id="CargarRegistros">
                          <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
               </div>

           </div>

           <br>

            <div class='col-md-12' >
              <div id='tabla_parametros' class="table-responsive"></div>
            </div>


      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <div class="control-sidebar-bg"></div>
	</form>

  <!-- REQUIRED JS SCRIPTS -->
  @yield('js')
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

      	<script type="text/javascript">
             var listaProcesos = [];
      	    $( document ).ready(function() {

              var $dfd = $.Deferred();
                   $('#tabla_parametros').jtable({
                                title: 'Catalogo de Parametros',
                                paging: true,
                                sorting: true,
      				                  pageSize: 30,
                                pageSizeChangeArea: false,
                                defaultSorting: 'CLAVEPARAMETRO ASC',
                                gotoPageArea: 'none',
      						              useBootstrap: true,
                                ajaxSettings: {
                                    type: 'GET'
                                },
                                actions: {
                                         listAction: '/crud_parametro?accion=list',
                                         updateAction: '/crud_parametro?accion=update',
                                         deleteAction:'/crud_parametro?accion=delete',
                                         createAction:'/crud_parametro?accion=create',
                                },
                                fields: {
                                       CLAVEPROCESO: {
                                         title: 'Clave Proceso',
                                         width: '20%',
                                         key: true,
                                         list: true,
                                         create: true,
                                         edit: true,
																				 inputClass: 'validate[required]',
                                         options:<?php
                                                  $cons_usuario="WEBUSER";
                                                  $cons_contra="W2b88gC4rp";
                                                  $cons_base_datos="DWH_Interfaces";
                                                  $cons_equipo="10.0.101.10";

                                                  $serverName = "$cons_equipo\sqlexpress, 1433"; //serverName\instanceName, portNumber (por defecto es 1433)

                                                  $connectionInfo = array( "Database"=>$cons_base_datos, "UID"=>$cons_usuario, "PWD"=>$cons_contra,"CharacterSet" => "UTF-8");
                                                  $obj_conexion = sqlsrv_connect( $serverName, $connectionInfo);
                        												  $consulta="SELECT * FROM SAPQAL.DEV.dev.ZTBL_PROCESOS order by CLAVEPROCESO";
                                                  $consulta2="Select COUNT(*) AS RecordCount from SAPQAL.DEV.dev.ZTBL_PROCESOS";
                                                  $stmt2=sqlsrv_query($obj_conexion, $consulta2);
                                                  $row1 = sqlsrv_fetch_array($stmt2);
                                                  $n = $row1['RecordCount'];

                        												  $stmt=sqlsrv_query($obj_conexion, $consulta);

                        												  echo "{";
                            												$i=0;

                            												while($row = sqlsrv_fetch_array($stmt)){
                            													$i++;
                            													echo "'".$row['CLAVEPROCESO']."':'".$row['DESCRIPCION']."'";

                            													if($i<$n){
                            														echo ",";
                            													}
                            												}
                          												echo "}";

                                                    sqlsrv_free_stmt($stmt);
                                                    sqlsrv_free_stmt($stmt2);
                                                    sqlsrv_close($obj_conexion);
                                              	?>

                                        },
                                        CLAVEPARAMETRO: {
                                          title: 'Clave Parametro',
                                          width: '20%',
																					key: true,
                                          list: true,
 																				  inputClass: 'validate[required]',
                                          create: true,
                                          input: function (data) {
                                               if (data.value) {
                                                   return '<input type="text" readonly class="jtable-input-readonly" name="JobId" value="' + data.value + '"/>';
                                               } else {
                                                   return '<input type="text" readonly class="jtable-input-readonly" name="JobId" value="Autogenerado"/>';
                                               }
                                           },

                                          inputClass: 'validate[required]'
                                         },
                                        DESCRIPCION: {
                                                 title: 'Descripción Parametro ',
                                                 width: '60%',
                                                 list: true,
                                                 create: true,
                                                 edit: true,
              								                   inputClass: 'validate[required]'
                                        }

              									}

              					

                    });

                   $('#CargarRegistros').click(function (e){
              				e.preventDefault();
                      $('#tabla_parametros').jtable({ajaxSettings: { type: 'GET', cache:false }});

              				$('#tabla_parametros').jtable('load', {
              					fl_clave: $('#claveParametro').val()
              				});
            			});

      			      $('#CargarRegistros').click();

          });
    </script>

@endsection
