@extends('voyager::master')

@section('content')

<style>

	.bg-primary {
			background-color: #19B5FE;
			color : white;
	}

	table.dataTable thead tr {
	  background-color: #82CFFD;
	color : black;
	}

	div.container {
	        width: 100%;
	    }

	form {
	  max-width:100%;
	  min-width:100%;

	  font-family:raleway;
	  background-color:#fff
	}
	p {
	  margin-top:0%
	}

	th { font-size: 12px; }
	td { font-size: 11px; }

  jtable-input-readonly{
    background-color:lightgray;
  }

</style>
	<form action="#" id="form" method="post" name="form">
<h2 class="modal-title" align="center" style="padding-top:2%" name="EstadoId" id="EstadoId">Catalogo de Procesos</h2>

	  <div class="row">
      <div class="modal-header">
        <div class="btn-group pull-right" >
           <button type="button" class="btn btn-primary " data-dismiss="modal" id="exit" name="exit" >Regresar Listado Configuraciones</button>
           <button type="button" class="btn btn-danger " id="menu" name="menu" onclick="">Regresar Menú</button>

        </div>

			</div>

    </div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content" style=" padding-left:2%">

           <div class='col-md-6'>

               <div class="input-group" >
                 <input type='text' class='form-control' id='clave' placeholder='Escribe la clave del proceso'>
                    <div class="input-group-btn" >
                      <button class="btn btn-info" type='button' id="CargarRegistros">
                          <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
               </div>

           </div>

           <br>

            <div class='col-md-12' >
              <div id='tabla_procesos' class="table-responsive"></div>
            </div>


      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <div class="control-sidebar-bg"></div>
	</form>

  <!-- REQUIRED JS SCRIPTS -->
  @yield('js')
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

      	<script type="text/javascript">
             var listaProcesos = [];
      	    $( document ).ready(function() {

              var $dfd = $.Deferred();
                   $('#tabla_procesos').jtable({
                                title: 'Catalogo de Procesos',
                                paging: true,
                                sorting: true,
      				                  pageSize: 30,
                                pageSizeChangeArea: false,
                                defaultSorting: 'CLAVEPROCESO ASC',
                                gotoPageArea: 'none',
      						              useBootstrap: true,
                                ajaxSettings: {
                                    type: 'GET'
                                },
                                actions: {
                                         listAction: '/crud_proceso?accion=list',
                                         updateAction: '/crud_proceso?accion=update',
                                         deleteAction:'/crud_proceso?accion=delete',
                                         createAction:'/crud_proceso?accion=create',
                                },
                                fields: {
                                       CLAVEPROCESO: {
                                         title: 'Clave Proceso',
                                         width: '20%',
                                         key: true,
                                         list: true,
																				 inputClass: 'validate[required]',
                                         create: true,
                                         input: function (data) {
                                              if (data.value) {
                                                  return '<input type="text" readonly class="jtable-input-readonly" name="JobId" value="' + data.value + '"/>';
                                              } else {
                                                  return '<input type="text" readonly class="jtable-input-readonly" name="JobId" value="Autogenerado"/>';
                                              }
                                          },

                                         inputClass: 'validate[required]'
                                        },
                                        DESCRIPCION: {
                                                 title: 'Descripción Proceso ',
                                                 width: '60%',
                                                 list: true,
                                                 create: true,
                                                 edit: true,
              								                   inputClass: 'validate[required]'
                                        }
                              
              									}



                    });

                   $('#CargarRegistros').click(function (e){
              				e.preventDefault();
                      $('#tabla_procesos').jtable({ajaxSettings: { type: 'GET', cache:false }});

              				$('#tabla_procesos').jtable('load', {
              					fl_clave: $('#clave').val()
              				});
            			});

      			      $('#CargarRegistros').click();

          });
    </script>

@endsection
