<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/reportes', function () {
        return view('vendor.voyager.reportes.browse');
    })->name('menuFiscal');

Route::get('/listasNegras', function () {
        return view('vendor.voyager.reportes.listasNegras.index');
    })->name('listaNegra');

   Route::get('/listaDetalle', function () {
        return view('vendor.voyager.reportes.listasNegras.detalle_view');
    })->name('listaDetalle');

    Route::get('/historicoDetalle', function () {
        return view('vendor.voyager.reportes.listasNegras.detalleHistorico_view');
    })->name('listaHistorico');

    Route::get('listasJson', function () {
        return view('vendor.voyager.reportes.listasNegras.data');
    });

    Route::get('listaNegraModal', function () {
        return view('vendor.voyager.reportes.listasNegras.listaModal');
    });

    Route::get('listaHistoricoDetalle', function () {
        return view('vendor.voyager.reportes.listasNegras.listaModalHistoricoDetalle');
    });


    Route::get('listaNegraHistorica', function () {
        return view('vendor.voyager.reportes.listasNegras.listaModalHistorica');
    });

    Route::get('listaProcesosModal', function () {
        return view('vendor.voyager.reportes.listasNegras.telefonias');
    });

    Route::get('listaControlProcesos', function () {
        return view('vendor.voyager.reportes.listasNegras.listaProcesos_view');
    })->name('listaProcesos')->middleware('csrf');

    Route::get('getCombosTel', function () {
        return view('vendor.voyager.reportes.listasNegras.combosTelefonia');
    });

    Route::get('tipo/{type}', 'SweetController@notification');

    Route::get('/listaWebSAT', function () {
      return view('vendor.voyager.reportes.listasNegras.listadoCompletoSAT');
    })->name('listaSAT');


    Route::get('listadoSAT', function () {
      return view('vendor.voyager.reportes.listasNegras.listaCompletaSAT');
    });

//catalogo de procesos
    Route::get('/listaProcesos', function () {
         return view('vendor.voyager.reportes.retenciones.procesos_view');
     })->name('listaProcesos');

    Route::get('/crud_proceso', function () {
      return view('vendor.voyager.reportes.retenciones.crud_procesos');
    });

//catalogo de parametros por proceso
    Route::get('/listaParametros', function () {
         return view('vendor.voyager.reportes.retenciones.parametros_view');
     })->name('listaParametros');

    Route::get('/crud_parametro', function () {
      return view('vendor.voyager.reportes.retenciones.crud_parametros');
    });

//catalogo tabla ZTBL_CTASRET cuentas a mayor
        Route::get('/cuentaMayor', function () {
             return view('vendor.voyager.reportes.retenciones.ctamayor_view');
         })->name('cuentaMayor');

        Route::get('/crud_ctamayor', function () {
          return view('vendor.voyager.reportes.retenciones.crud_ctamayor');
        });


    //catalogo de parametros por proceso por usuario
        Route::get('/listaProcUsuarios', function () {
             return view('vendor.voyager.reportes.retenciones.procuser_view');
         })->name('listaProcUsuarios');

        Route::get('/crud_procusuario', function () {
          return view('vendor.voyager.reportes.retenciones.crud_procusuarios');
        });

        //calendario por proceso por usuario
          //  Route::get('/scheduler', function () {
          //       return view('vendor.voyager.eventos.calendario_view');
          //   })->name('scheduler');

            Route::get('/crud_calendario', function () {
              return view('vendor.voyager.eventos.crud_calendario');
            });

        //catalogo de parametros por proceso configuracion
            Route::get('/listaProcConfig', function () {
                 return view('vendor.voyager.reportes.retenciones.procconfig_view');
             })->name('listaProcConfig');

            Route::get('/crud_procconfig', function () {
              return view('vendor.voyager.reportes.retenciones.crud_procconfig');
            });

            Route::get('/getUserSesion', function () {
              return view('vendor.voyager.reportes.retenciones.response');
            });
            Route::get('/updateJitterBit', function () {
              return view('vendor.voyager.sistemas.jitterbit.actualiza_proceso');
            });

            Route::get('/inconsistencialistaNegra', function () {
              return view('vendor.voyager.sistemas.ListasNegras.tblListasnegras');
            })->name('inconsistencialistaNegra');

            Route::get('/crud_tblListasnegras', function () {
              return view('vendor.voyager.sistemas.ListasNegras.crud_tblListasnegras');
            });
